## How to get the code work in Eclipse?

The repo contains Eclipse projects that can directly be imported into the workspace.

Two projects contain 3rd party libraries:

1. Hibernate_Libs
2. Postgres_Driver

When set as dependencies in the build path of other Eclipes Java projects, they serve as drivers,
and no additional libraries have to be downloaded in order to clear the assignment.

Luckily, the third project is already setup for you:

* JPAStandaloneExample

Import all three. Try to make the JPAStandaloneExample work by executing the DatabasePopulator. Once you've got it working, you can experiment with it by adjusting it to your desired object model, your file import routine, and your database import routine too.
